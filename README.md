# 学習記録

## Doing
- https://www.udemy.com/graphql-with-react-course
    - [graphql-with-react-course - 1](https://gitlab.com/HayatoKamono/graphql-with-react-the-complete-developers-guide_1) 

## Done

## Pending
- https://frontendmasters.com/courses/advanced-graphql/
- https://frontendmasters.com/courses/api-node-rest-graphql/
- https://www.pluralsight.com/courses/graphql-scalable-apis
- https://www.pluralsight.com/courses/react-apps-with-relay-graphql-flux
- https://egghead.io/courses/build-a-graphql-server
 

# Wiki
- [https://gitlab.com/HayatoKamono/graphql_index/wikis/home](https://gitlab.com/HayatoKamono/graphql_index/wikis/home)